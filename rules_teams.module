<?php

/**
 * @file
 * Main module file.
 */

/**
 * Implements hook_help().
 */
function rules_teams_help($path, $arg) {
  switch ($path) {
    case 'admin/help#rules_teams':
      $output = '<h3>Rules for Teams</h3>';
      $output .= '<p>Provides a way for the Rules module to send messages to Microsoft Teams. Only a limited subset of what is available in Teams is currently supported.</p>';
      $output .= '<p><a href="https://www.drupal.org/project/rules_teams">module page on drupal.org</a></p>';
      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function rules_teams_permission() {
  return array(
    'configure rules_teams defaults' => array(
      'title' => t('Configure Rules for Teams defaults.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function rules_teams_menu() {
  $items['admin/config/rules_teams'] = array(
    'title' => 'Rules for Teams',
    'description' => 'Configure Rules for Teams module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rules_teams_configure_form'),
    'access arguments' => array('configure rules_teams defaults'),
  );
  return $items;
}

/**
 * Function to set up a configuration form.
 *
 * Called by rules_teams_menu().
 */
function rules_teams_configure_form() {
  $form = array();

  $form['rules_teams_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of Teams incoming webhook'),
    '#default_value' => variable_get('rules_teams_url'),
    '#description' => t('Removing this URL will prevent sending to Teams.'),
    '#size' => 225,
    '#maxlength' => 255,
  );

  return system_settings_form($form);
}

/**
 * Validation function for rules_teams_configure_form().
 */
function rules_teams_configure_form_validate($form, &$form_state) {
  $url = $form_state['values']['rules_teams_url'];
  if ($url && !preg_match('/^https:\/\/outlook.office.com\/webhook\/.*\/IncomingWebhook\/.*/', $url)) {
    // Note: preg_match is very basic since I don't know the rules for the URLs.
    form_set_error('rules_teams_configure_form', t('You must provide a valid https://outlook.office.com/webhook/.../IncomingWebhook/... URL.'));
  }
}

/**
 * Implements hook_rules_action_info().
 */
function rules_teams_rules_action_info() {
  $actions = array();
  $actions['rules_teams_send_message'] = array(
    'label' => t('Teams send message'),
    'group' => t('Microsoft Teams'),
    'parameter' => array(
      'title' => array(
        'type' => 'text',
        'label' => t('Message title'),
        'optional' => TRUE,
      ),
      'message' => array(
        'type' => 'text',
        'label' => t('Message to send'),
      ),
      'color' => array(
        'type' => 'text',
        'label' => t('Theme color'),
        'description' => t('A 6-character hex value. Microsoft says not to use this colour to impart meaning.'),
        'optional' => TRUE,
      ),
    ),
  );
  return $actions;
}

/**
 * Function to validate rules form submission.
 *
 * Called by rules_team_rules_action_info().
 * Info on how to do this from comment at https://www.drupal.org/node/878940.
 */
function rules_teams_send_message_validate($element) {
  if (!preg_match('/^[a-f0-9]{6}$/i', $element->settings['color'])) {
    throw new RulesIntegrityException(
        t('Theme color must be a valid 6-character hex value.'),
        array($element, 'parameter', 'color')
      );
  }
}

/**
 * Function to send message to Microsoft Teams.
 *
 * Called by rules_teams_action_rules_info().
 */
function rules_teams_send_message($title, $message, $color) {
  $url = variable_get('rules_teams_url');
  if ($url && $message) {
    // Compose message.
    $teams_message['@type'] = 'MessageCard';
    $teams_message['@context'] = 'http://schema.org/extensions';
    if ($color) {
      $teams_message['themeColor'] = $color;
    }
    if ($title) {
      $teams_message['title'] = $title;
    }
    $teams_message['text'] = $message;
    $teams_message = json_encode($teams_message);
    // Send message.
    $options = array(
      'http' => array(
        'method' => 'POST',
        'content' => $teams_message,
        'header' => "Content-Type: application/json\r\nAccept: application/json\r\n",
      ),
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, FALSE, $context);
  }
}
